Benefits / Features to emphasize:
*********************************
no training, animated heatmaps, live progress, export/excel/csv, API/PowerBi compatibility, easy-to-use, cheap/low cost, pay-as-you-use, per-project pricing, benchmarking, AI powered dashboard, multiple device (iOS, Android), customization options, Quick & Easy setup, extensive analytics dashboard, instant results, comprehensive reports, Speed/fast, transparent pricing, user-specific permissions, 

"implementation in 3 days or less" (setup speed)
"Cloud based  SaaS with free upgrades"
"No license fee for unlimited users"

Advantage to competitors:
- we track activites and technologies and observers are able to add notes
- 

Not used descriptions:
- Fast Observations
    300 observations per hour? No problem. Our game-like observation apps allow fast observations with minimal training.
- Detailed reporting
    Our instant report dashboard allows you to get a quick overview or drill down to the exact things you want to know.
- Quick and Easy Setup
    Be done with endless planning sessions. With Vantage Space you can set up a full study in (almost) no time.
- Customizable
    Define which types of workspaces, activities and technologies you want to track. Adjust colors and make it all your own.
- way more (live progress, export, API)
    Track the progress of ongoing studies, export your data or integrate your own solution with our rest-based API.
- Track Activities and Technologies
    Define the workspaces, activities and technologies you want to track. No unnecessary fluff.
Powerful Analytics
    Our detailed analytics dashboards (incl. tables, charts and heatmaps) give you in-depth information and fulfill every demand.
- Affordable Pricing (!)
    Our transparent pricing system lets you know instantly how much it will cost. Bulk purchases qualify for higher discounts.
- Use your Existing Devices
    Use any browser to set up your study. Our observation apps support a wide range of Ipad and Android devices.
- Instant results
    Watch the live progress of your studies, get alerted if your observers need help and and check your results in real time - even while the study is running.

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Pricing page:
    - Unlimited users
    - no license fee
    - free trial
    
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Footer:

Contact Us
*************
Chat Online
Email us

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Country List

*United States, *United Kingdom, *Australia, *Canada, China,
*Sweden, *Netherlands, *Singapore, *Germany, *India,
*New Zealand, *Hong Kong, *Mexico, *Denmark, *Spain
Poland, *Italy, *Switzerland, *Japan, *France
Argentina, Russia, Colombia, Czechia, Chile
*Brazil, South Korea, *Ireland, Turkey, *Norway
Ukraine, Austria, Egypt, Israel, Hungary
*Finland, Philippines, Thailand, South Africa, Indonesia
Belgium, Peru, United Arab Emirates, Taiwan, Vietnam
*Romania, Malaysia, Slovakia, Lithuania, Luxembourg
Greece, Panama, Latvia, Bangladesh, Uruguay
Saudi Arabia, Ecuador, Portugal, Sri Lanka", Georgia
Paraguay, Cambodia, Bolivia, Malta, North Korea, 
Tanzania, Kenya, Belize, Jamaica, Qatar
Costa Rica, Pakistan

72 Countries

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Additionally discussed marketing messages to implement:

Corona-related phrasing:
	- "What is happening in your building? - Find out now!" 
	- "Cutting through chaos, what's happening right now, what's happening on the ground"
	- "Future changing daily, studies will help inform."
	- Uses VS to come up with a plan quickly
add to website:
    - offline observations
    - we need to add more numbers (90% less time, 82% less cost)
	- Tells clients that vs sensors VS has- accuracy, more layers to it, quantitive and qualitive and pitches it as such. 
	- User friendly apps, temps can be trained in 1 hour, pick up and go. 
	- Tell employees if leaving desk, pick up stuff and put in a locker. Observers using SoL status helps promote the conversations to solve office layouts. Why was a desk always SoL etc etc.
    
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Color Scheme Vantage Space:
teal (#1DAFAF),
dark teal (#007C7C),
yellow (#FFFF33),
red (#FF3724),
light red(#FF9486),
orange(#FF9500),
light orange(#FFD540),
green(#4CD964),
light green(#7FFF97),
purple(#A1459F),
light purple(#D68CD5),
blue(#007AFF),
light blue(#4DC7FF)

Color Scheme Vantage Space:
.occupied {
  border-color: #FF3B30;
  background-color: #FF9489;
}
.signs_of_life {
  border-color: #FF9500;
  background-color: #FFD540;
}
.unoccupied {
  border-color: #4CD964;
  background-color: #7FFF97;
}
.unusable {
  border-color: #007AFF;
  background-color: #4DC7FF;
}
.unobservable {
  border-color: #858585;
  background-color: #AFAFAF;
}
.vacant {
  border-color: #A1459F;
  background-color: #D68CD5;