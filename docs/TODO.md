Seb TODO:
* select, adapt & add After Effects isometric images / animations
* look through Temp notes for added comments to include on the website

Karl TODO:
* review current content
* replace Pay-as-you-use with cost-saving dashboard text copy + image (cost savings dashboard: "we offer features and graphs that identifies the biggest cost savings for you.")
* change text copy of last section (focus: low cost)
* change meta data
		* some stuff needs to be extracted into a variable
		* middle man uses config files
* update netlify headers (redirections)
* find old pricing page FAQ & review current pricing-page FAQ
* buy/hook up some Twilio numbers around the world
* cancel/refund policy: https://www.semrush.com/company/legal/refund-policy/#cancellation-policy

Heather TODO:
* select, adapt and flat images
* fix top nav issues when minimised.

TODO:
* inline vector graphics as base64 https://gist.github.com/jamesdarvell/9ee60056244325b74083
* Remove all crazy data tags (difficult)
* Mix in these marketing points: simple setup, quick to setup/speed, no fixed tour/route, data is yours, project-based pricing, Cushman charge 2x our pricing, Measuremen charge 3x our pricing (and don't like to give data)
* Design graphic ala: https://vantagespace.slack.com/archives/G1BTGABE2/p1584099805000100
* Fix GA conversion tracking (see assets/javascripts)
* Move industry_terms.yml from old site and create new page on new site
* Move testimonials.yml from old site
* Move faqs-pricing.yml from old site and create new page on new site
* 404 page
* hook up intercom-new partial
* move everything out of static to the appropriate place
* hook up /press
* fix og:image and other TODOs in meta-tags
* extract good copywriting content from other sites
* good country changer:
	https://www.uber.com/it/en/
	https://www.revolut.com/en-IT/change-country/
	https://www.apple.com/choose-country-region/
	https://www.salesforce.com/eu/?ir=1
	<link rel="alternate" hreflang="en-uk" href="<%= current_url_for_locale(:uk) -%>" />
	<link rel="alternate" hreflang="en-us" href="<%= current_url_for_locale(:us) -%>" />
	<link rel="alternate" hreflang="en-au" href="<%= current_url_for_locale(:au) -%>" />
	<link rel="alternate" hreflang="en-ca" href="<%= current_url_for_locale(:ca) -%>" />
	<link rel="alternate" hreflang="en-nz" href="<%= current_url_for_locale(:nz) -%>" />
	<meta property="og:locale:alternate" content="en_AU" />
	<meta property="og:locale:alternate" content="en_CA" />
	<meta property="og:locale:alternate" content="en_NZ" />
	<meta property="og:locale:alternate" content="en_UK" />
	<meta property="og:locale:alternate" content="en_US" />

----------------

Add redirects back in:

# https://www.netlify.com/docs/redirects/
# You can find a list of country codes here: http://dev.maxmind.com/geoip/legacy/codes/iso3166/
# And language codes here: http://www.metamodpro.com/browser-language-codes
# For languages, note that en will match en-US and en-GB, zh will match zh-tw and zh-hk, etc.

# Test here: https://play.netlify.com/redirects

# EXAMPLES:
# Redirect users in China, Hongkong or Taiwan to /china.
# there CANNOT be spaces in the last parameter: Country=x,y,z or Language=xx,yy
# /  /china   302  Country=cn,hk,tw
# Redirect users in israel to /israel
# /  /israel  302  Country=il

# Redirect users with chinese language preference from /china to /china/zh-cn
# /china/*  /china/zh-cn/:splat  302  Language=zh

# Redirects from old website URLs
# https://play.netlify.com/redirects
/augmented-reality          https://vantagespace.wistia.com/medias/6r6xmubruu       302
/credits                    https://my.vantagespace.com/credits                     301
/free                       https://my.vantagespace.com/sign_up                     302
/business-case              /downloads/Vantage-Space-Business-Case.pdf              302
/business-case-calculator   /downloads/Vantage-Space-Business-Case-Calculator.xlsx  302
/api                        /downloads/Vantage-Space-API.pdf                        302
/enterprise                 /downloads/Vantage-Space-Enterprise.pdf                 302

/blog  /au/   302  Country=au
/blog  /ca/   302  Country=ca
/blog  /eu/   302  Country=eu,ad,al,at,ba,be,bg,by,ch,cs,cz,de,dk,ee,es,fi,fo,fr,fx,gr,hr,hu,ie,is,it,li,lt,lu,lv,mc,md,mk,mt,nl,no,pl,pt,ro,se,si,sj,sk,sm,ua,va
/blog  /nz/   302  Country=nz
/blog  /uk/   302  Country=gb,gi
/blog  /us/   302  Country=us,um,vi,pr

/love  /au/customers   301  Country=au
/love  /ca/customers   301  Country=ca
/love  /eu/customers   301  Country=eu,ad,al,at,ba,be,bg,by,ch,cs,cz,de,dk,ee,es,fi,fo,fr,fx,gr,hr,hu,ie,is,it,li,lt,lu,lv,mc,md,mk,mt,nl,no,pl,pt,ro,se,si,sj,sk,sm,ua,va
/love  /nz/customers   301  Country=nz
/love  /uk/customers   301  Country=gb,gi
/love  /us/customers   301  Country=us,um,vi,pr

/product  /au/features   301  Country=au
/product  /ca/features   301  Country=ca
/product  /eu/features   301  Country=eu,ad,al,at,ba,be,bg,by,ch,cs,cz,de,dk,ee,es,fi,fo,fr,fx,gr,hr,hu,ie,is,it,li,lt,lu,lv,mc,md,mk,mt,nl,no,pl,pt,ro,se,si,sj,sk,sm,ua,va
/product  /nz/features   301  Country=nz
/product  /uk/features   301  Country=gb,gi
/product  /us/features   301  Country=us,um,vi,pr

# Please note that "EU" and "AP" codes are only used when a specific country code has not been designated (see FAQ). Blocking or re-directing by "EU" or "AP" will only affect a small portion of IP addresses. Instead, you should list the countries you want to block/re-direct individually.
# See full list here: http://dev.maxmind.com/geoip/legacy/codes/iso3166/
# See Europe list here: https://dev.maxmind.com/geoip/legacy/codes/eu_country_list/
/  /au/   302  Country=au
/  /ca/   302  Country=ca
/  /eu/   302  Country=eu,ad,al,at,ba,be,bg,by,ch,cs,cz,de,dk,ee,es,fi,fo,fr,fx,gr,hr,hu,ie,is,it,li,lt,lu,lv,mc,md,mk,mt,nl,no,pl,pt,ro,se,si,sj,sk,sm,ua,va
/  /nz/   302  Country=nz
/  /uk/   302  Country=gb,gi
/  /us/   302  Country=us,um,vi,pr


<!-- TODO <meta name="twitter:image" content="https://www.tunnelbear.com/static/images/social-meta/share_graphic.jpg" id="twitter-image"> -->

<!-- TODO <meta property="og:image" content="static/images/social-meta/share_graphic.jpg">
 -->
<!-- TODO <meta property="og:image:width" content="1200"> -->
<!-- TODO <meta property="og:image:height" content="630"> -->

<!-- TODO <meta itemprop="image" content="static/images/social-meta/share_graphic.jpg"> -->
<!-- TODO <meta name="msapplication-TileColor" content="#715942"> -->
<!-- TODO <meta name="msapplication-TileImage" content="mstile-144x144.html"> -->
<!-- TODO <meta name="theme-color" content="#ffffff"> -->
<!-- TODO <link rel="mask-icon" href="safari-pinned-tab.html" color="#454f5d"> -->
<!-- TODO <script type="application/ld+json" id="schema-page">
  {
    "@context": "http://schema.org/",
    "@type": "SoftwareApplication",
    "applicationCategory": "WebApplication",
    "applicationSubCategory": "VPN Service",
    "name": "TunnelBear",
    "operatingSystem": "IOS, ANDROID, OSX, PC, CHROME",
    "downloadUrl": "https://www.tunnelbear.com/download",
    "description": "Simply VPN privacy apps for every device.",
    "screenshot": "https://www.tunnelbear.com/static/images/mac-app.png"
  }
</script> -->
<!-- TODO <script type="application/ld+json" id="schema-organization">
  {
    "@context": "http://schema.org",
    "@type": "Organization",
    "name": "TunnelBear",
    "url": "https://www.tunnelbear.com",
    "logo": "https://www.tunnelbear.com/static/images/tunnelbear-logo.png",
    "sameAs": [
      "https://www.facebook.com/tunnelbear",
      "https://www.twitter.com/thetunnelbear",
      "https://www.linkedin.com/company/tunnelbear"
    ]
  }
</script> -->


-----


favicons file

<!-- TODO
  <link rel="apple-touch-icon" sizes="57x57" href="apple-touch-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="apple-touch-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="apple-touch-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="apple-touch-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="apple-touch-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="apple-touch-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="apple-touch-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="apple-touch-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon-180x180.png">
  <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32">
  <link rel="icon" type="image/png" href="favicon-194x194.png" sizes="194x194">
  <link rel="icon" type="image/png" href="favicon-96x96.png" sizes="96x96">
  <link rel="icon" type="image/png" href="android-chrome-192x192.png" sizes="192x192">
  <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16">
  -->

site.webmanifest
	{
	+  "name": "",
	+  "short_name": "",
	+  "icons": [{
	+    "src": "/android-chrome-192x192.png",
	+    "sizes": "192x192",
	+    "type": "image/png"
	+  }, {
	+    "src": "/android-chrome-512x512.png",
	+    "sizes": "512x512",
	+    "type": "image/png"
	+  }],
	+  "theme_color": "#ffffff",
	+  "background_color": "#ffffff",
	+  "display": "standalone"
	+}
