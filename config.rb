# Activate and configure extensions
# https://middlemanapp.com/advanced/configuration/#configuring-extensions

activate :autoprefixer do |prefix|
  prefix.browsers = 'last 2 versions'
end

# This generates pretty URLS, like /about rather than /about.html and does this
# by generating about/index.html and leaving it up to the server
# https://middlemanapp.com/advanced/pretty-urls/
activate :directory_indexes

# https://middlemanapp.com/advanced/localization/
# For performance reasons we switch off other languages during frontend development
LANGS = %i[global de_DE es_ES zh_CN au ca eu nz uk us]
activate :i18n, langs: LANGS

activate :sitemap, hostname: @app.data.site.address.production # https://github.com/middleman/middleman/issues/1810

ignore '*industry-terms*'
ignore '*press*'
ignore '*choose-country-region*'

configure :development do
  # ignore /blog\/posts\/201[0-6]-[0-9][0-8].*.html/       # ignore everything before 2016-09
  # ignore /blog\/posts\/201[0-6]-[0-9][0-9]-[^2].*.html/  # ignore everything in september 2016 unless in the 20s.
  # ignore "/assets/fonts/*"
  # ignore "/assets/video/video-1.ogv"
end

# command: build? ? 'npm run build' : 'npm run start',
# configure :production do
activate :external_pipeline,
         name: :webpack,
         command: if build?
                    'node ./node_modules/webpack/bin/webpack.js --bail -p'
                  else
                    'node ./node_modules/webpack/bin/webpack.js --watch -d --progress --color'
                  end,
         source: '.tmp/dist',
         latency: 1
# end

# Also note that we’re overriding the default :js_dir and :css_dir configuration. Even when using the :external_pipeline, Middleman will attempt to process files in source/assets/* (e.g. uglify them), so we neatly sidestepped that issue without the need for complex ignore rules.
config[:js_dir] = 'assets/javascripts'
config[:css_dir] = 'assets/stylesheets'

# Layouts
# https://middlemanapp.com/basics/layouts/

# Per-page layout changes
page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false

# With alternative layout
# page '/path/to/file.html', layout: 'other_layout'

# Proxy pages
# https://middlemanapp.com/advanced/dynamic-pages/

# proxy(
#   '/this-page-has-no-template.html',
#   '/template-file.html',
#   locals: {
#     which_fake_page: 'Rendering a fake page with a local variable'
#   },
# )

# Helpers
# Methods defined in the helpers block are available in templates
# https://middlemanapp.com/basics/helper-methods/

# helpers do
#   def some_helper
#     'Helping'
#   end
# end

# Build-specific configuration
# https://middlemanapp.com/advanced/configuration/#environment-specific-settings

# configure :build do
#   activate :minify_css
#   activate :minify_javascript
# end

# configure :server { #enable sprockets debugging }
# configure :build do # run some post-build hooks
#   import_file File.expand_path("_headers", config[:source]), "/_headers.erb"
# end
# configure :development { # enable some sass debug settings }
# configure :production { activate :minify_html }
