module I18nLinks
  def price_i18n(price)
    money = if t('setting.currency_symbol_after')
      "#{price}#{t('setting.currency')}"
    else
        "#{t('setting.currency')}#{price}"
    end
    money.gsub("000","#{t('setting.thousand_separator')}000")
  end

  def url_i18n(url, loc = I18n.locale)
    # extensions[:i18n].localized_path(slug, I18n.locale)
    # url_for_locale(url, I18n.locale)
    if url == '/' && loc == :global
      '/'
    elsif url == '/' && loc != :global
      '/' + loc.to_s
    elsif url != '/' && loc == :global
      # url_for(url)
      '/' + t("paths.#{url}", locale: loc)
    elsif url != '/' && loc != :global
      '/' + loc.to_s + '/' + t("paths.#{url}", locale: loc)
    else
      raise 'Should never get here'
    end
  end

# -----

def current_path_for_locale(loc = I18n.locale, is_link = true)
  return 'javascript: void(0);' if is_link && I18n.locale == loc
  url_regex = /\A\/(?:(#{I18n.available_locales.join('|')})\/)?/
  current_page.url.gsub(url_regex, '').blank? ?
      home_for_locale(loc) :
      current_page.url.gsub(url_regex, root_for_locale(loc))
end

def path_for_locale(path, loc = I18n.locale)
  root_for_locale(loc) + path
end

def home_for_locale(loc = I18n.locale)
  root_for_locale(loc)
end

def root_for_locale(loc = I18n.locale)
  loc == :global ? '/' : "/#{loc}/"
end

# -----
# https://stackoverflow.com/questions/52025314/switch-to-the-another-language-on-the-same-page
  def current_url_for_locale(loc)
    url_for_locale(current_page.url, loc)
    # path_for_locale(current_url_for_translation, loc)
  end

  def url_for_locale(url, loc)
    url_regex = /\A\/(?:(#{I18n.available_locales.join('|')})\/)?/
    locale_root = url_for('/', locale: loc)
    url.gsub(url_regex, '').blank? ?
        locale_root :
        current_page.url.gsub(url_regex, locale_root)
  end
# -----
end
