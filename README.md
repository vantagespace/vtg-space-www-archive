# Vantage Space marketing website

## Getting started developing

### One-time setup

Linux/Mac install:
1. bundle install
2. npm install

Windows install:
* Follow tutorial: https://middlemanapp.com/basics/install/
1. Install Ruby 2.6.2 (v2.6.2 is the maximum netlify supports, we can’t update yet)
        - leave "Run 'ridk install'[...]" checked at the end of the installation
        - in console popup choose 3 ("MSYS2 and MINGW development toolchain") and press enter to install MSYS2
        - if "pacman failed" error: choose option 2 ("MSYS2 system update"), then 3 again
        - open command line window (cmd), type 'ruby --version' to check successful install
2. install middleman
        - cmd, type 'gem install middleman"
        - cmd, type 'middleman version' to check successful install
3. install node.js
4. cmd: bundle install
5. cmd: npm install
6. Test rake server
        - cmd, go to project folder
        - type 'rake server'

Possible Errors-Handling:
a) libcurl.dll not found
        - Take a libcurl.dll from one of the packages found here: https://curl.haxx.se/download.html#Win64 (for 64 bit version, rename libcurlx64.dll to libcurl.dll)
        - put it under \ruby24\bin\ (32-bit) OR \ruby24-x64\bin (64-bit)

----

Install netlify CLI:
https://www.netlify.com/docs/cli/
npm install netlify-cli -g
netlify login
netlify --telemetry-disable
netlify sites:list

### On-going development

1. rake server -OR- netlify dev
2. open http://localhost:4567

See 'rake -T' for more commands.

---

# Sort/tidy this (taken from old site)

* Tracking/analytics
  - Google Analytics properly: funnels/conversions, ads/PPC, remarketing
  - Check Adroll/FB/Twitter/LinkedIn

Browser support
---------------
The website should support:
* iOS/iPadOS - Safari
* Android Mobile/Tablet - Chrome
* Desktop - Chrome, Firefox, Safari, IE/Edge

Psychology
----------
> Target audiences: Large Companies and Property-Related Consultancies. The main target is Consultancies.
> They want to know: "What's going on in this building?" and we need to be clear we can provide an answer to that question.
> They'll ask themselves: "Is this a new tool in my toolbox?"
> They're looking for work after the study, specifically: "From the study, what can I do here to make money later?" -> "What services can I justify offering?" -> "What data supports this?"

Misc
----

Generator used for our client map
https://pixelmap.amcharts.com

Preferred icon websites:
https://www.flaticon.com (use Tor/VPN to bypass download limit)
https://thenounproject.com

Emojis as HTML entities
https://gist.github.com/oliveratgithub/0bf11a9aff0d6da7b46f1490f86a71eb


Signup links
------------

Sign up links are extracted into a partial to ensure they hook up with google analytics regardless of classes applied.
<%= partial('sign_up_button', :locals => { class_params: "btn d-block mt-4 btn-outline-primary", text_params: "Get Started" }) -%>

Both parameters are optional, defaults can be seen in source/_sign_up_button.haml, default is a large button like the one at the top-right of the header.

---

* Tracking/analytics
  - Google Analytics properly: funnels/conversions, ads/PPC, remarketing
  - Check Adroll/FB/Twitter/LinkedIn

Browser support
---------------
The website should support:
* iOS/iPadOS - Safari
* Android Mobile/Tablet - Chrome
* Desktop - Chrome, Firefox, Safari, IE/Edge

Psychology
----------
> Target audiences: Large Companies and Property-Related Consultancies. The main target is Consultancies.
> They want to know: "What's going on in this building?" and we need to be clear we can provide an answer to that question.
> They'll ask themselves: "Is this a new tool in my toolbox?"
> They're looking for work after the study, specifically: "From the study, what can I do here to make money later?" -> "What services can I justify offering?" -> "What data supports this?"

Misc
----

Generator used for our client map
https://pixelmap.amcharts.com

Preferred icon websites:
https://www.flaticon.com (use Tor/VPN to bypass download limit)
https://thenounproject.com

Emojis as HTML entities
https://gist.github.com/oliveratgithub/0bf11a9aff0d6da7b46f1490f86a71eb


Signup links
------------

Sign up links are extracted into a partial to ensure they hook up with google analytics regardless of classes applied.
<%= partial('sign_up_button', :locals => { class_params: "btn d-block mt-4 btn-outline-primary", text_params: "Get Started" }) -%>

Both parameters are optional, defaults can be seen in source/_sign_up_button.haml, default is a large button like the one at the top-right of the header.


Adding new languages
--------------------
* Find correct locale: http://www.i18nguy.com/unicode/language-identifiers.html
* Add to Crowdin
* Add to config.rb
* rake translations:download
