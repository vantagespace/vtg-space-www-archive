task default: [:server]

desc 'Run local development server'
task :server do
  sh 'bundle exec middleman server -e development'
end

namespace :build do
  # task default: [:development]
  desc 'Build for development (but do not deploy)'
  task :development do
    sh 'bundle exec middleman build -e development'
    # Manually copy netlify files over, as middleman ignores files
    # starting with _ and this cannot be overriden
    sh 'cp netlify/_headers_development build/_headers'
    # sh 'cp netlify/_redirects build/_redirects'
  end
  desc 'Build for production (but do not deploy)'
  task :production do
    sh 'bundle exec middleman build -e production'
    # Manually copy netlify files over, as middleman ignores files
    # starting with _ and this cannot be overriden
    # sh 'cp netlify/_redirects build/_redirects'
  end
end
task build: 'build:development'

namespace :test do
  desc 'Run all tests for development'
  task :development do
    Rake::Task['build:development'].invoke
    Rake::Task['html_proofer'].invoke
    Rake::Task['translations:test'].invoke
  end
  desc 'Run all tests for production'
  task :production do
    Rake::Task['build:production'].invoke
    Rake::Task['html_proofer'].invoke
    Rake::Task['translations:test'].invoke
  end
end
task test: 'test:development'

task :html_proofer do
  require 'html-proofer'
  raise IOError, 'Directory ./build does not exist. Run `rake build` before running tests' unless Dir.exist?('./build')

  HTMLProofer.check_directory('./build', {
                                check_img_http: true,
                                check_html: true, validation: { report_missing_names: true },
                                check_favicon: true,
                                check_opengraph: true,
                                allow_hash_href: false,
                                url_ignore: [%r{/free$}, %r{^http://googleads\.g\.doubleclick\.net}],
                                http_status_ignore: [0, 999, 403, 401]
                              }).run
  # Check out the config docs for more: https://github.com/gjtorikian/html-proofer#configuration
end

namespace :translations do
  # require 'crowdin-api'
  # require 'logger'
  # require 'pp'
  # require 'zip'
  # require 'yaml'
  CROWDIN_API_KEY = 'c21bbc458e5c7b8ea806abe71ad3a4d2'
  CROWDIN_PROJECT_ID = 'vtg-space-www'
  # crowdin.log = Logger.new $stderr
  desc 'Upload master content file to Crowdin for translation'
  task :upload do
    crowdin = Crowdin::API.new(api_key: CROWDIN_API_KEY, project_id: CROWDIN_PROJECT_ID)
    puts 'Uploading: locales/global/content.yml'
    crowdin.update_file(
      files = [
        {
          source: 'locales/global/content.yml',
          dest: 'content.yml',
          title: 'Website content for translation',
          export_pattern: 'locales/%locale_with_underscore%/%original_file_name%'
        }
      ]
    )
    puts 'Check out: https://crowdin.com/project/vtg-space-www'
  end
  desc 'Download translated files from Crowdin'
  task :download do
    crowdin = Crowdin::API.new(api_key: CROWDIN_API_KEY, project_id: CROWDIN_PROJECT_ID)
    puts 'Downloading: tmp/all.zip'
    # Build the translations
    # n.b. this will work only ever 30 mins,
    crowdin.export_translations
    # Download the translations
    crowdin.download_translation('all', output: 'tmp/all.zip')
    Zip::File.open('tmp/all.zip') do |zip_file|
      puts 'Opening: tmp/all.zip'
      zip_file.glob('**/*.yml').each do |entry|
        source = entry.name
        dirname = File.dirname(entry.name)
        locale = dirname
        locale.sub!(/-/, '_') # Fix Crowdin not underscoring the locales
        locale = 'de_DE' if locale == 'de' # Fix Crowdin locale for German German
        filename = File.basename(entry.name)
        destination = "locales/#{locale}/#{filename}"
        puts "Extracting: #{source} to #{destination}"
        # Rewrite YAML root to #{language}
        content = entry.get_input_stream.read
        content.sub!(/^global:/, "#{locale}:")
        File.open(destination, 'w+') do |file|
          file.write(content)
        end
      end
    end
    # File.delete('tmp/all.zip')
    # pp crowdin.translations_status
  end
  desc 'QA the translation files'
  task :test do
    # Check no keys in source but not targets
    # Check no keys in targets but not source
    # Check lang:paths: are lowercase (they create the URLs)
    # Check for 'translation missing' in any of the output files
    Dir['build/**/*.html'].each do |filename|
      puts "Checking for missing translations: #{filename}"
      search = File.readlines(filename).grep(/translation missing/)
      if search.size > 0
        puts "Missing translation found in: #{filename}"
        pp search
      end
    end
  end
end
