FROM ruby:2.6.2

RUN apt-get update -qq && apt-get install -y nodejs

RUN curl -fsSL https://deb.nodesource.com/setup_14.x | bash - && \
  apt-get update && apt-get install -y nodejs

ENV APP_HOME /app
WORKDIR $APP_HOME

ENV LANG=C.UTF-8 \
  BUNDLE_JOBS=4 \
  BUNDLE_RETRY=3

RUN gem install bundler

COPY . $APP_HOME



RUN bundle install
RUN npm install

CMD rake server