---
global:
  paths:
    pricing: pricing
  content:
    signup_button: Start a Free Trial
    create_account_today: Get started with<br />Vantage Space today!
    cookies_message: We use cookies to make your online experience better. By continuing on our site, you're giving us consent to use cookies. For more information, see <a data-cookies="" href="%{url_privacy}" class="">our privacy policy</a>.
    countries:
      usa: USA
      uk: UK
      australia: Australia
      canada: Canada
      china: China
      sweden: Sweden
      netherlands: Netherlands
      singapore: Singapore
      germany: Germany
      india: India
      new_zealand: New Zealand
      hong_kong: Hong Kong
      mexico: Mexico
      denmark: Denmark
      spain: Spain
      poland: Poland
      italy: Italy
      switzerland: Switzerland
      japan: Japan
      france: France
      argentina: Argentina
      russia: Russia
      colombia: Colombia
      czechia: Czechia
  menu:
    pricing: Pricing
    support: Support
    account: My Account
    apps: Apps
    web_app: Web App
    company: Company
    terms: Terms of Service
    privacy: Privacy Policy
    follow: Follow Us
  page:
    index:
      title: Vantage Space
      hero_title: Identify savings and improvements in your workplace
      hero_subtitle: "Vantage Space is the world's #1 app for occupancy and space utilization."
      hero_stat_1: meeting room efficiency
      hero_stat_2: desk usage efficiency
      apps_title: Easy-to-use apps for all your devices
      apps_subtitle: Use any web browser to set up a study. Collect data with our iPhone, iPad, or Android apps. No training necessary.
      reasons_title: Why companies and consultancies choose us
      reasons_quick_title: Quick setup
      reasons_quick_description: With Vantage Space you can set up a full study in less than a day.
      reasons_easy_title: Easy to use
      reasons_easy_description: Download from the app store. Our app is fun and intuitive; no observer training needed.
      reasons_cost_title: Low cost
      reasons_cost_description: State-of-the-art tools for a fraction of the usual cost. <a href="%{url_pricing}">Per project pricing</a> with unlimited users.
      reasons_tracking_title: Activity and technology tracking
      reasons_tracking_description: Go beyond occupancy data, track what people are doing and what they are using.
      reasons_savings_title: Savings Dashboard
      reasons_savings_description: See which parts of your offices need optimizing and how much money it will save.
      reasons_analytics_title: AI-powered analytics
      reasons_analytics_description: The dashboard generates animated heatmaps and graphs, providing answers instantly.
      countries_title: Trusted by organizations in %{country_count} countries
      countries_description: Vantage Space is used by the world’s best recognised companies and consultancies around the world.
      features_title: The all-in-one solution - packed with powerful features
      features_devices_title: Use your existing devices
      features_devices_description: Easily set up your study on any computer. Our observation apps support both iOS and Android.
      features_reports_title: Instant reporting
      features_reports_description: With our reports dashboard you can see the results of your study immediately.
      features_data_title: Full data access
      features_data_description: Export your raw data (as CSV/XLSX) or use our API to connect with your own tools.
      savings_title: Cut your real estate costs
      savings_description: Optimize your office space and save unnecessary real estate costs.
      savings_1: Find unused workspaces and reallocate them
      savings_2: See which meeting rooms are underutilized and remodel them
      savings_3: Make evidenced-based decisions
      faq_title: Frequently Asked Questions
      faq_1_title: What is Vantage Space?
      faq_1_description: Vantage Space is an all-in-one space utilization survey tool that enables you to carry out space utilization and occupancy studies with ease and efficiency.
      faq_2_title: Which devices are supported?
      faq_2_description: Your web account is accessible with any browser. Observation apps are available for iPads, Android tablet and Android mobile.
      faq_3_title: How does Vantage Space work?
      faq_3_description: After setting up a study, your observers can conduct observations using our observation apps. The data is uploaded to your the web app which automatically generates heatmaps, graphs and charts to help you answer all your questions around your workplace.
      faq_4_title: How does Vantage Space keep my data secure?
      faq_4_description: Vantage Space encrypts all data with TLS/HTTPS. All of our databases (all located in AWS datacenters in Europe) are encrypted at rest. Access to data is protected by authentication and authorization controls. Third party security tools are used to scan for vulnerabilities.
      faq_5_title: Why should I choose Vantage Space over other solutions?
      faq_5_description: Save up to 90% of time and reduce your cost by up to 82% compared to other solutions. Set your study up in less than a day and get your answers now. No hardware installation required. Vantage Space is trusted by consultants and companies in 73 countries and 639 cities.
      faq_6_title: Another FAQ Title
      faq_6_description: Another FAQ Description
    pricing:
      title: Pricing
      hero_title: Pick the plan that's right for you
      small: Small
      small_workspaces: Up to 250 workspaces
      medium: Medium
      medium_workspaces: Up to 500 workspaces
      large: Large
      large_workspaces: Up to 1%{thousand_separator}000 workspaces
      per_study: "/study"
      get_started_button: Get started
      comes_with: "Every study comes with:"
      with_users: Unlimited users
      with_ios: iPhone & iPad app
      with_android: Android app
      with_dashboard: Analysis dashboard
      with_export: Data export
      with_api: API access
      with_weeks: Study up to 4 weeks
      with_support: Chat support
      custom_question: Interested in something different?
      custom_cta: Get in touch for custom pricing
      shown_in: "All pricing shown in %{currency_code}"
      testimonial_1_text: Now we are able to hire temporary interns to gather the data. And, because the user interface is so intuitive and friendly, we do not have to spend any time training them. Vantage Space has saved us lots of time and money.
      testimonial_1_author: Fortune 500 Workplace Consultant
      testimonial_2_text: "It is a simple to use app that engages the data-inputter, creating real-time statistics at a low cost... it allows anyone to understand how their workplace is being used and to identify opportunities to save money and improve employee experience."
      testimonial_2_author: Royal Institution of Chartered Surveyors (RICS)
      faq_title: Frequently Asked Questions
      faq_1_title: What is Vantage Space?
      faq_1_description: Vantage Space is an all-in-one space utilization survey tool that enables you to carry out space utilization and occupancy studies with ease and efficiency.
      faq_2_title: Which devices are supported?
      faq_2_description: Your web account is accessible with any browser. Observation apps are available for iPads, Android tablet and Android mobile.
      faq_3_title: How does Vantage Space work?
      faq_3_description: After setting up a study, your observers can conduct observations using our observation apps. The data is uploaded to your the web app which automatically generates heatmaps, graphs and charts to help you answer all your questions around your workplace.
      faq_4_title: How does Vantage Space keep my data secure?
      faq_4_description: Vantage Space encrypts all data with TLS/HTTPS. All of our databases (all located in AWS datacenters in Europe) are encrypted at rest. Access to data is protected by authentication and authorization controls. Third party security tools are used to scan for vulnerabilities.
      faq_5_title: Why should I choose Vantage Space over other solutions?
      faq_5_description: Save up to 90% of time and reduce your cost by up to 82% compared to other solutions. Set your study up in less than a day and get your answers now. No hardware installation required. Vantage Space is trusted by consultants and companies in %{country_count} countries and %{city_count} cities.
      faq_6_title: I have my own tool. Why should I switch to Vantage Space?
      faq_6_description: Developing and maintaining a software solution is time-consuming and costly. With Vantage Space you get the world's best solution for space utilization studies with high-end security standards, ongoing feature development and personal chat support for a fraction of the cost. Try it now for free.
    glossary:
      title: Workplace Industry Terms
